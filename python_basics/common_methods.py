"""Every python file is a module"""
"""Every folder that has a __init__.py is called a python package."""


# ToDo - review string formatting in python

def print_iterable(iterable):
    """Applies for list, tuple or set"""
    for element in iterable:
        print(element)


def print_dict(dictionary):
    for key, value in dictionary.items():
        print('{} value is  {}'.format(key, value))
