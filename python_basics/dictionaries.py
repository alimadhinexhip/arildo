import timeit

# Different ways to create a dictionary and benchmark their performance

human = {
    'age': 10,
    'height': 180.5,
    'eye_color': 'brown',
}
human2 = dict(age=10, height=180.5, eye_color='brown')
print(human)
print(human2)
print('Is humman variable equal to humman2 variable: {}'.format(human == human2))
print('\n')

time_first_way_to_create_human_dict = timeit.timeit("{'age': 10,'height': 180.5,'eye_color': 'brown'}", number=10000)
print('The first way to create a dict takes {} seconds'.format(time_first_way_to_create_human_dict))

time_second_way_to_create_human_dict = timeit.timeit("dict(age=10, height=180.5, eye_color='brown')", number=10000)
print('The second way to create a dict takes {} seconds'.format(time_second_way_to_create_human_dict))
print('The first way is faster than the second one!')
print('\n')

print('Create dictionary from 2 iterable sequences')

man = dict(zip(['age', 'height', 'eye_color'], ['11', 160, 2]))
man2 = {k: v for k, v in zip(['age', 'height', 'eye_color'], ['11', 160, 2])}
man3 = dict((k, v) for k, v in zip(['age', 'height', 'eye_color'], ['11', 160, 2]))

time_first_way_to_create_man_dict_from_2_iterables = timeit.timeit(
    "dict(zip(['age', 'height', 'eye_color'], ['11', 160, 2]))", number=10000)
time_second_way_to_create_man_dict_from_2_iterables = timeit.timeit(
    "{k: v for k, v in zip(['age', 'height', 'eye_color'], ['11', 160, 2])}", number=10000)
time_third_way_to_create_man_dict_from_2_iterables = timeit.timeit(
    "dict((k, v) for k, v in zip(['age', 'height', 'eye_color'], ['11', 160, 2]))", number=10000)

print('The first one has better performance than the other two')

print('The first way (using zip only) to create a dict where keys and values are iterables takes {} seconds'.format(
    time_first_way_to_create_man_dict_from_2_iterables))
print(
    'The second way (using dictionary comprehension) to create a dict where keys and values are iterables takes {} seconds'.format(
        time_second_way_to_create_man_dict_from_2_iterables))
print(
    'The third way (using dictionary comprehension and dict constructor explicitly) to create a dict where keys and values are iterables takes {} seconds'.format(
        time_third_way_to_create_man_dict_from_2_iterables))
