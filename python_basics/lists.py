from python_basics.common_methods import print_iterable

numbers = [1, 2, 3, 4]

print('First element of the list: ')
print(numbers[0])
print(numbers)
numbers.append(2345)
print('All elements of the list')

print_iterable(numbers)
