"""Three types of import

import os
import json as js
from python_basics.common_methods import print_iterable

"""
import os as o
from python_basics.common_methods import print_iterable

print('\n Tuples')
numbers_tuple = (1, 3, 5, 7, 9)

print('\n Before adding')
print_iterable(numbers_tuple)
print('\n After adding')
numbers_tuple += (1234,)
print_iterable(numbers_tuple)
print(o.path)
